import PropTypes from "prop-types";

import "./MovieItem.css";
import { useState } from "react";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  // TODO create a "favorite" state, default value : false
  const [favorite, setFavorite] = useState(false);
  function toggleFavorite() {
    if (favorite) {
      setFavorite(false);
    } else {
      setFavorite(true);
    }
  }

  // TODO add an action when clicking on the favorite button

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      <button type="button" onClick={toggleFavorite}>
        {favorite ? "❤️" : "🤍"}
      </button>
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
